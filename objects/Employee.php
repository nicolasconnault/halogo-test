<?php
namespace Halogo\Dataobjects;
class Employee extends DataObject {

    public $id;
    public $first_name;
    public $last_name;
    public $job_title;
    public $department;
    public $email_address;

    public function __construct(int $id, string $first_name, string $last_name, string $job_title, string $department, string $email_address) {
        $this->id = $id;
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->job_title = $job_title;
        $this->department = $department;
        $this->email_address = $email_address;
    }

    public static function getList(string $department = '', int $page = 1, int $records_per_page = 10) {

        // in final application, it would retrieve data from the database instead of CSV file
        $file = fopen(__DIR__."/../staff.csv", "r");
        $employees = array();

        while (($data = fgetcsv($file, 1000, ",")) !== false) {
            $employees[] = new Employee((int) $data[0], $data[1], $data[2], $data[3], $data[4], $data[5]);
        }

        $sorted_employees = Employee::sort_employees($employees);

        $filtered_employees = array();

        foreach ($sorted_employees as $key => $employee) {
            if (strlen($department) > 0 && $employee->department != $department) {
                continue;
            }

            $filtered_employees[] = $employee;
        }

        $paged_employees = Employee::get_paged_records($filtered_employees, $page, $records_per_page);
        return $paged_employees;
    }

    public static function sort_employees($employees) {
        usort($employees, fn($a, $b) => strcmp($a->last_name, $b->last_name));
        return $employees;
    }
}
