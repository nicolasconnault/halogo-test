<?php
namespace Halogo\Dataobjects;
abstract class DataObject {
    public static function get_paged_records(Array $records, int $page, int $records_per_page) {
        if ($page < 1) {
            $page = 1;
        }

        if ($records_per_page < 1) {
            $records_per_page = 1;
        }

        $paged_records = array();

        $first_record_key = ($page * $records_per_page) - $records_per_page;
        $last_record_key = ($page * $records_per_page) - 1;

        foreach ($records as $key => $record) {
            if ($key >= $first_record_key and $key <= $last_record_key) {
                $paged_records[] = $record;
            }
        }

        return $paged_records;
    }
}
