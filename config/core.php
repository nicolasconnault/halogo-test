<?php
//show error reporting
ini_set('display_errors', 1);
error_reporting(E_ALL);

//home page url
$home_url="http://halogo/php-rest-api/";

//page given in url parameter, default page is one
$page = isset($_REQUEST['page']) ? $_REQUEST['page'] : 1;

//set number of records per page
$records_per_page = 10;
