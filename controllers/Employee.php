<?php
namespace Halogo\Controllers;

class Employee {
    public static $records_per_page = 10;
    public function allList() {
        // Accept POST named parameters or GET request
        $department = (isset($_REQUEST['department'])) ? $_REQUEST['department'] : '';
        $page = (isset($_REQUEST['page'])) ? $_REQUEST['page'] : 1;

        $employees = \Halogo\Dataobjects\Employee::getList($department, $page, \Halogo\Controllers\Employee::$records_per_page);

        //If more than 0 records
        if (count($employees) > 0) {
            return json_encode($employees);
        } else {
            return json_encode(array(
                "message" => "No employees found.")
            );
        }

    }
}
