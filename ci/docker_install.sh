#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe
apt-get update && apt-get install -y --fix-missing \
    apt-utils \
    gnupg

echo "deb http://packages.dotdeb.org jessie all" >> /etc/apt/sources.list
echo "deb-src http://packages.dotdeb.org jessie all" >> /etc/apt/sources.list
curl -sS --insecure https://www.dotdeb.org/dotdeb.gpg | apt-key add -

apt-get update && apt-get install -y \
    zlib1g-dev \
    libzip-dev \
    zip
docker-php-ext-install zip

# We can enable extensions with this simple line. We need the ZIP extension to run for Composer
# docker-php-ext-enable zip
curl --silent --show-error https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# If your tests need a special php.ini setting, you can copy you own php.ini file here.
# This is useful for instance to increase the memory limit
cp ci/php.ini /usr/local/etc/php/php.ini

# Let's run composer install
composer install
