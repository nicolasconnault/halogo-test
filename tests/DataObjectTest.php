<?php
declare(strict_types = 1);

use PHPUnit\Framework\TestCase;

final class DataObjectTest extends TestCase {

    private $sample_array = array('record 1', 'record 2', 'record 3', 'record 4', 'record 5', 'record 6', 'record 7', 'record 8', 'record 9');

    public function testHandlesWrongTypes(): void {
        $this->expectException(TypeError::class);

        \Halogo\Dataobjects\DataObject::get_paged_records(1, 2, 3);
        \Halogo\Dataobjects\DataObject::get_paged_records(null, 2, 3, 4);
    }

    public function testHandlesSubOneArguments(): void {
        $this->assertEquals(
            array('record 1'),
            \Halogo\Dataobjects\DataObject::get_paged_records($this->sample_array, -3, -1)
        );
    }

    public function testReturnsPagedRecords(): void {
        $this->assertEquals(
            array('record 3', 'record 4'),
            \Halogo\Dataobjects\DataObject::get_paged_records($this->sample_array, 2, 2)
        );
        $this->assertEquals(
            array('record 1', 'record 2', 'record 3', 'record 4', 'record 5'),
            \Halogo\Dataobjects\DataObject::get_paged_records($this->sample_array, 1, 5)
        );
    }
}

