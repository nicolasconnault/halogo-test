<?php
declare(strict_types = 1);

use PHPUnit\Framework\TestCase;

final class EmployeeTest extends TestCase {

    public function testSortRecords(): void {
        $sample_employees = array();
        $sample_employees[] = (object) ['last_name' => 'Zerus'];
        $sample_employees[] = (object) ['last_name' => 'Jones'];
        $sample_employees[] = (object) ['last_name' => 'Bloggs'];

        $expected_employees = array();
        $expected_employees[] = (object) ['last_name' => 'Bloggs'];
        $expected_employees[] = (object) ['last_name' => 'Jones'];
        $expected_employees[] = (object) ['last_name' => 'Zerus'];

        $sorted_employees = \Halogo\Dataobjects\Employee::sort_employees($sample_employees);
        $this->assertEquals(
            $sorted_employees, $expected_employees
        );
    }
}
