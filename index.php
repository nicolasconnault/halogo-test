<?php
require_once('vendor/autoload.php');
use Scrawler\Router\RouteCollection;
use Scrawler\Router\Router;
use Symfony\Component\HttpFoundation\Response;

$dir = 'controllers/';
$namespace = 'Halogo\Controllers';

$router = new Router(new RouteCollection($dir,$namespace));
//Optional you can now pass your own Request object to Router for Router to work on
//$router = new Router(new RouteCollection($dir,$namespace),Request $request);


//Dispatch route and get back the response
$response = $router->dispatch();

//Do anything with your Response object here
//Probably middleware can hook in here

//send response
$response->send();
